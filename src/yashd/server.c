#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#include <signal.h>

#include <unistd.h>
#include <fcntl.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <netdb.h>

#include <arpa/inet.h>
#include <pty.h>

#include <event2/event.h>
#include <event2/buffer.h>
#include <event2/listener.h>
#include <event2/bufferevent.h>

#include "../evycp/ycp.h"
#include "../utils.h"

static
void print_addr(const char* fmt, struct sockaddr* sas, socklen_t len)
{  
    int port;
    struct in_addr addr;
    char host[INET6_ADDRSTRLEN];


    port = ntohs(((struct sockaddr_in*)sas)->sin_port);
    addr = ((struct sockaddr_in*)sas)->sin_addr;

    inet_ntop(sas->sa_family, &addr, host, sizeof host);
    fprintf(stderr, fmt, host, port);
}

struct yash_session {
    struct bufferevent *sockbev;
    struct bufferevent *ptybev;

    /*
     * PID of the spawned shell. It is included here for means of reaping
     * and additionally checking for abnormal exits (exitcode nonzero).
     */
    pid_t shellpid;
};

static void bev_tx_data(struct bufferevent *bev, void *ctx)
{
    struct yash_session* session = ctx;
    int ret;

    if(session->sockbev == bev)
        bufferevent_read_buffer(bev, bufferevent_get_output(session->ptybev));
    else
        bufferevent_read_buffer(bev, bufferevent_get_output(session->sockbev));
}

static void free_session(
        struct bufferevent *bev,
        short what,
        void *ctx
) {
    struct yash_session* session = ctx;

    int shellpid_stat;
    int ret;

    if(what & (BEV_EVENT_EOF | BEV_EVENT_ERROR)) {
        /**
         * Once we close the master end of the PTY, slave side recieves a
         * SIGHUP to all the processes in the slave's process and those
         * who choose to continue shall remain alive.
         */
        bufferevent_free(session->ptybev);
        bufferevent_free(session->sockbev);

        ret = waitpid(session->shellpid, &shellpid_stat, 0);
        if(ret == 0) {
            if(WIFEXITED(shellpid_stat)) {
                // TODO: report this to client possibly?
            } else if(WIFSIGNALED(shellpid_stat)) {
                //       and also this?
                // well maybe just syslog 'em for debugging
            }
        }

        free(session);
    }
}

static void allocate_session(
        uint32_t inspeed,
        uint32_t outspeed,
        const char* user,
        const char* ruser,
        const char* term,
        void* arg
) {
    int slave, master, ret;
    struct yash_session *session = arg;

    master = bufferevent_getfd(session->ptybev);

    // TODO: set (inspeed, outspeed) accordingly,
    //though i think these are only for legacy.

    session->shellpid = fork();
    if(session->shellpid == 0) {
        static const int errfd = STDERR_FILENO + 1;
        static char TERM_env[65535];

        ret = grantpt(master);
        if(ret < 0) {
            perror("fatal: grantpt()");
            exit(-1);
        }

        unlockpt(master);
        slave = open(ptsname(master), O_RDWR);
        if(slave < 0) {
            perror("fatal: slave pty open");
            exit(-1);
        }
        close(master);

        /*
         * If this process is a daemon, it has no controlling TTY.
         * But run setsid() to be sure it has no one.
         *
         * In order to set, the recently allocated PTY it must be
         * be a session leader and have a controlling TTY which is
         * the slave side of the allocated PTY in server instance
         * where is process is spawned off of.
         */
        setsid();
        ret = ioctl(slave, TIOCSCTTY, 0);
        if(ret < 0) {
            perror("fatal: ioctl_tty()");
            exit(-1);
        }

        /*
         * The standard IO streams refer to its controlling TTY
         * which is a full-duplex stream. STDOUT and STDERR are
         * are intermerged (eg. in xterm or Linux console).
         */
        dup2(slave, STDIN_FILENO);
        dup2(slave, STDOUT_FILENO);
        dup2(slave, STDERR_FILENO);

        /*
         * The purpose of keeping alive the stderr fd until exec(2)
         * is for reporting if exec(2) failed, for better debugging.
         */
        dup2(STDERR_FILENO, errfd);
        flmod(errfd, O_CLOEXEC);

        strcpy(TERM_env, "TERM=");
        strcat(TERM_env, term);

        // TODO: perform the authentication.
        execve("/bin/bash",
               (char*[]){"/bin/bash", NULL},
               (char*[]){TERM_env, NULL});

        /*
         * All suceeding exec(2) calls never reach to this code, and when they
         * do they indicate that an error has happened. Though use syslog().
         */
        dup2(errfd, STDERR_FILENO);
        perror("fatal: exec()");
        exit(-1);
    } else if(session->shellpid < 0) {
        perror("fatal: fork()");

        free_session(NULL, BEV_EVENT_ERROR, session);
        return;
    }
}

static void update_winsize(uint16_t cols, uint16_t rows, void* fd) {
    int ret;

    ret = ioctl((int)(size_t)fd, TIOCSWINSZ, &(struct winsize){
              .ws_col = cols,
              .ws_row = rows
          });
    if(ret < 0)
        perror("warn: ioctl()");
}

static void on_client_accept(
    struct evconnlistener *evcl,
    evutil_socket_t        sock,
    struct sockaddr       *addr,
    int                    addrlen,
    void                  *ctx
) {
    int ret, child, master;
    struct event_base *base;
    struct evycp *evycp;
    struct yash_session *session;

    print_addr("info: client connected %s:%d\n", addr, addrlen);

    session = malloc(sizeof(struct yash_session));
    if(!session) {
        fputs("fatal: malloc()", stderr);

        close(sock);
        return;
    }

    master = posix_openpt(O_RDWR | O_NOCTTY);
    if(master < 0) {
        perror("fatal: posix_openpt()");

        free(session);
        close(sock);
        return;
    }

    // the socket must be non-blocking and closed when called exec(2)
    // why would this be alive in the child?
    ret = flmod(master, O_NONBLOCK | O_CLOEXEC);
    if(ret < 0)
        perror("warn: flmod()");

    base = evconnlistener_get_base(evcl);

    session->sockbev = bufferevent_socket_new(base,
                                              sock,
                                              BEV_OPT_CLOSE_ON_FREE);
    if(!session->sockbev) {
        fputs("fatal: bufferevent_socket_new()", stderr);

        free(session);
        close(master);
        close(sock);
        return;
    }

    evycp = evycp_server_new(session->sockbev,
                             &session->sockbev,
                             BEV_OPT_CLOSE_ON_FREE,

                             allocate_session, session,
                             update_winsize, (void*)(uintptr_t)master);

    if(!evycp) {
        fputs("fatal: evycp_sevrer_new()", stderr);

        bufferevent_free(session->sockbev);
        free(session);
        close(master);
        return;
    }

    session->ptybev = bufferevent_socket_new(base,
                                             master,
                                             BEV_OPT_CLOSE_ON_FREE);
    if(!session->ptybev) {
        fputs("fatal: bufferevent_socket_new()", stderr);

        bufferevent_free(session->sockbev);
        free(session);
        close(master);
        return;
    }

    bufferevent_setcb(session->sockbev,
                      bev_tx_data,
                      NULL,
                      free_session,
                      session);
    bufferevent_setcb(session->ptybev,
                      bev_tx_data,
                      NULL,
                      free_session,
                      session);

    ret  = bufferevent_enable(session->ptybev, EV_READ | EV_WRITE);
    ret += bufferevent_enable(session->sockbev, EV_READ | EV_WRITE);
    if(ret < 0) {
        fputs("fatal: bufferevent_enable()", stderr);

        bufferevent_free(session->ptybev);
        bufferevent_free(session->sockbev);
        free(session);
        return;
    }
}

static
void print_usage(const char* argv0)
{
    fprintf(stderr, "usage: %s [hostname] [port]\n", argv0);
    exit(EXIT_FAILURE);
}

static void on_server_error(struct evconnlistener *evconn, void *ctx)
{
    struct event_base* base = evconnlistener_get_base(evconn);

    event_base_loopbreak(base);
    event_base_loopexit(base, NULL);
}

int main(argc, argv)
    int    argc;
    char **argv;
{
    struct addrinfo *resaddr;
    struct event_base* base;
    struct evconnlistener* serverevcl;
    int ret;

    if(argc < 3)
        print_usage(argv[0]);

    ret = getaddrinfo(argv[1], argv[2], NULL, &resaddr);
    if(ret < 0) {
        if(ret == EAI_SYSTEM)
        {
            perror("fatal: getaddrinfo()");
            return -errno;
        }
        else
        {
            fputs(gai_strerror(ret), stderr);
            return -ret;
        }
    }

    base = event_base_new();
    if(!base) {
        fputs("fatal: evconnlistener_new_bind()", stderr);

        freeaddrinfo(resaddr);
        return -1;
    }

    serverevcl = evconnlistener_new_bind(base,
                                         on_client_accept,
                                         NULL,
                                         LEV_OPT_REUSEABLE |
                                         LEV_OPT_CLOSE_ON_EXEC |
                                         LEV_OPT_CLOSE_ON_FREE,
                                         -1,
                                         resaddr->ai_addr,
                                         resaddr->ai_addrlen);

    if(!serverevcl) {
        fputs("fatal: evconnlistener_new_bind()", stderr);

        event_base_free(base);
        freeaddrinfo(resaddr);
        return -1;
    }

    evconnlistener_set_error_cb(serverevcl, on_server_error);

    ret = evconnlistener_enable(serverevcl);
    if(ret < 0) {
        fputs("fatal: evconnlistener_enable()", stderr);

        evconnlistener_free(serverevcl);
        event_base_free(base);
        freeaddrinfo(resaddr);
        return -1;
    }

    ret = event_base_dispatch(base);
    if(ret < 0)
        fputs("fatal: event_base_dispatch()", stderr);

    evconnlistener_free(serverevcl);
    freeaddrinfo(resaddr);
    event_base_free(base);
    libevent_global_shutdown();

    if(ret < 0)
        return -1;
}
