#include <stdlib.h>
#include <string.h>

#include <termios.h>
#include <unistd.h>
#include <fcntl.h>

int ctl_tty_fd;
struct termios ctl_tty_tios;

int make_ctl_tty_raw() {
    int ret;
    struct termios ctl_tty_new_tios;

    ctl_tty_fd = open("/dev/tty", 0);
    if(ctl_tty_fd < 0) {
        // this means we had no controlling TTY to begin with.
        return 0;
    }

    ret = tcgetattr(ctl_tty_fd, &ctl_tty_tios);
    if(ret < 0) {
        ctl_tty_fd = -1;
        return -1;
    }

    memcpy(&ctl_tty_new_tios, &ctl_tty_tios, sizeof(struct termios));

    /** These obscure settings are copied from netkit-rsh code.
        There's not much proper documentation on this. */
    ctl_tty_new_tios.c_oflag &=~(ONLCR | OCRNL);
    ctl_tty_new_tios.c_lflag &=~(ECHO | ICANON | ISIG);
    ctl_tty_new_tios.c_iflag &=~ICRNL;

    ctl_tty_new_tios.c_cc[VTIME] = 0;
    ctl_tty_new_tios.c_cc[VMIN] = 0;

    if((ctl_tty_new_tios.c_oflag & TABDLY) == TAB3)
        ctl_tty_new_tios.c_oflag &= ~TAB3;

    ret = tcsetattr(ctl_tty_fd, TCSADRAIN, &ctl_tty_new_tios);
    if(ret < 0) {
        ctl_tty_fd = -1;
        return -1;
    }

    return 0;
}

void reset_ctl_tty() {
    if(ctl_tty_fd > -1) {
        // at this stage, we can't help if it doesn't succeed.
        tcsetattr(ctl_tty_fd, TCSADRAIN, &ctl_tty_tios);
    }
}
