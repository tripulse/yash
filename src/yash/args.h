#ifndef CLIENT_ARGS_H
#define CLIENT_ARGS_H

#include <stdio.h>
#include <string.h>
#include <unistd.h>

struct client_args {
    char* host;
    char* port;
    char* ruser;
};

int client_args_parse(struct client_args* args, int argc, char** argv) {
    char* optstring;
    char* ruser_host;
    int err;

    // IANA hasn't taken its hands on this port.
    args->port = "2194";

    err = 0;
    optind = 1;
    optstring = "p:";

    for(int opt = '\0'; (opt = getopt(argc, argv, optstring)) != -1;)
        switch(opt) {
        case 'p':
            args->port = optarg;
            break;

        case '?':
            err = -1;
            break;

        // this is not possible or is it?
        default:
            fprintf(stderr, "fatal: getopt(): returned charcode %c", opt);
            err = -1;
        }

    if(optind < argc) {
        ruser_host = argv[optind];

        args->ruser = strtok(ruser_host, "@");
        args->host  = strtok(NULL, "@");

        // this means, same as the local user is the ruser.
        if(!args->host) {
            args->host = args->ruser;
            args->ruser = NULL;
        }
    } else {
        fprintf(stderr, "fatal: host (with ruser) unspecified");
    }

    return err;
}

#endif // CLIENT_ARGS_H
