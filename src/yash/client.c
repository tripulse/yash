#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <unistd.h>
#include <fcntl.h>

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/types.h>

#include <pwd.h>
#include <netdb.h>

#include <signal.h>

#include <event2/event.h>
#include <event2/buffer.h>
#include <event2/bufferevent.h>

#include "../evycp/ycp.h"
#include "rawtty.h"
#include "../utils.h"
#include "args.h"

static void write_to_socket(
        evutil_socket_t fd,
        short what,
        void *arg
) {
    struct bufferevent* bev = arg;
    struct evbuffer* evb = bufferevent_get_output(bev);

    evbuffer_read(evb, fd, -1);
}

static void write_to_stdout(
        struct bufferevent *bev,
        void *ctx
) {
    struct evbuffer* in = bufferevent_get_input(bev);
    evbuffer_write(in, STDOUT_FILENO);
}

static void end_session(struct bufferevent *bev, short what, void *ctx) {
    if(what & (BEV_EVENT_EOF | BEV_EVENT_ERROR)) {
        struct event_base* base = bufferevent_get_base(bev);

        event_base_loopbreak(base);
        event_base_loopexit(base, NULL);
    }
}

static int init_ycp(struct evycp* ctx, const char* ruser) {
    struct passwd* pw;
    const char* term;
    const char* user;

    pw = getpwuid(getuid());
    if(pw == NULL)
        return -1;

    user = pw->pw_name;

    speed_t ispeed = 0,
            ospeed = 0;

    if(ctl_tty_fd > -1) {
        ispeed = cfgetispeed(&ctl_tty_tios);
        ospeed = cfgetospeed(&ctl_tty_tios);
    }

    // TODO: what is a good default TERM value?
    term = getenv("TERM");
    if(term == NULL)
        term = "";

    return evycp_client_init(ctx, ispeed, ospeed, user,
                             ruser ? ruser : user, term);
}

static void send_winsize(evutil_socket_t sig, short what, void *arg)
{
    struct winsize wsize;
    int ret;

    ret = ioctl(ctl_tty_fd, TIOCGWINSZ, &wsize);
    if(ret < 0)
        return;

    evycp_client_send_winch(arg, wsize.ws_col, wsize.ws_row);
}

int main(int argc, char** argv) {
    struct client_args args;
    struct termios oldtios, newtios;

    struct event_base *base;
    struct event *stdinev;
    struct event *winchev;
    struct evbuffer *connoutevb;
    struct bufferevent *connbev;

    struct evycp* ycpctx;
    struct addrinfo *resaddr;

    int ret, conn;

    ret = client_args_parse(&args, argc, argv);
    if(ret < 0)
        return -1;

    ret = getaddrinfo(args.host, args.port, NULL, &resaddr);
    if(ret < 0) {
        if(ret == EAI_SYSTEM) {
            perror("fatal: getaddrinfo()");
            return -errno;
        }
        else {
            fprintf(stderr, "fatal: getaddrinfo(): %s", gai_strerror(ret));
            return -ret;
        }
    }

    conn = socket(resaddr->ai_family, SOCK_STREAM, 0);
    if(conn < 0) {
        perror("fatal: socket()");
        return -errno;
    }

    ret = connect(conn, resaddr->ai_addr, resaddr->ai_addrlen);
    freeaddrinfo(resaddr);
    if(ret < 0) {
        perror("fatal: connect()");
        return -errno;
    }

    /*
     * If we do not reset the PTY with tcsetattr() our terminal will
     * be fucked up, like it wouldn't perform as normally it would.
     */
    atexit(reset_ctl_tty);
    ret = make_ctl_tty_raw();
    if(ret < 0) {
        perror("fatal: make_ctl_tty_raw()");
        return -ret;
    }

    ret = flmod(conn, O_NONBLOCK);
    if(ret < 0)
        perror("warn: flmod()");

    ret = flmod(STDIN_FILENO, O_NONBLOCK);
    if(ret < 0)
        perror("warn: flmod()");

    ret = flmod(STDOUT_FILENO, O_NONBLOCK);
    if(ret < 0)
        perror("warn: flmod()");

    base = event_base_new();
    if(!base) {
        fprintf(stderr, "fatal: failed event_base init");
        return -1;
    }

    connbev = bufferevent_socket_new(base, conn, BEV_OPT_CLOSE_ON_FREE);
    ycpctx = evycp_client_new(connbev, &connbev, BEV_OPT_CLOSE_ON_FREE);
    if(!ycpctx) {
        fprintf(stderr, "fatal: evycp_client_new()");

        bufferevent_free(connbev);
        event_base_free(base);

        return -1;
    }
    connoutevb = bufferevent_get_output(connbev);

    // though SIGWINCH is not a standard UNIX signal, but any other way of
    // being notified of this change other than polling is impossible.
    winchev = evsignal_new(base,
                           SIGWINCH,
                           send_winsize,
                           ycpctx);

    stdinev = event_new(base,
                        STDIN_FILENO,
                        EV_READ | EV_PERSIST,
                        write_to_socket,
                        connbev);

    bufferevent_setcb(connbev, write_to_stdout, NULL, end_session, NULL);
    bufferevent_enable(connbev, EV_READ | EV_WRITE);

    init_ycp(ycpctx, args.ruser);
    send_winsize(SIGWINCH, EV_SIGNAL, ycpctx);

    event_add(stdinev, NULL);
    event_add(winchev, NULL);
    event_base_dispatch(base);

    event_free(winchev);
    event_free(stdinev);
    bufferevent_free(connbev);
    event_base_free(base);
    libevent_global_shutdown();
}
