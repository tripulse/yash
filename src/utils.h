#ifndef UTILS_H
#define UTILS_H

#include <fcntl.h>

int flmod(int fd, int newflags) {
    int flags;

    flags = fcntl(fd, F_GETFL);
    if(flags < 0)
        return -1;

    return fcntl(fd, F_SETFL, flags | newflags);
}

#endif // UTILS_H
