#ifndef EVYCP_H
#define EVYCP_H

#include <stdint.h>

#include <event2/bufferevent.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Opaque structure to evycp client or server instance.
 * 
 * @note The contents of this structure are subject to change,
 * so relying on the contents might break ABI compatibility.
 */
struct evycp;

/**
 * Callback of this typedef called when the server side recieves a
 * Window Change (WINCH) notification from the client.
 * 
 * @param columns number of columns in size
 * @param rows    number of rows in size
 * @param arg     user argument given
 */
typedef void(*winch_cb_t)(
        uint16_t columns,
        uint16_t rows,
        void* arg);

/**
 * Callback of this typedef called when the server side gets initialised.
 * 
 * @param ispeed input baudrate of the remote terminal
 * @param ospeed output baudrate of the remote terminal
 * @param user   username connecting to the remote
 * @param ruser  remote username to login in as
 * @param term   terminal type requested
 * @param arg    user argument given
 */
typedef void(*init_cb_t)(
        uint32_t ispeed,
        uint32_t ospeed,
        const char* user,
        const char* ruser,
        const char* term,
        void* arg);

/**
 * Create a new client instance. It creates a filter buffervent which wraps 
 * over the underlying bufferevent and carries the client instance throughout
 * its lifetime.
 * 
 * @param underlying bufferevent to wrap over
 * @param filter filter that is created
 * @param filter_options filter options if any @see buffervent_filter_new()
 */
struct evycp* evycp_client_new(struct bufferevent* underlying,
                               struct bufferevent** filter,
                               int filter_options);

/**
 * Inititalise the client. This must be the first operation done on
 * the client instance. It sends a INIT packet to the server,
 * that contains information for session initialisation.
 * 
 * @param ctx client instance to operate on
 * @param inspeed input baudrate of the remote terminal
 * @param outspeed output baudrate of the remote terminal
 * @param user username to indentify as
 * @param ruser remote username to login as
 * @param term terminal type requested
 * 
 * @note user, ruser and term variable's combined length must be less
 * than 65527 or else it wouldn't truncate and fail.
 */
int evycp_client_init(
        struct evycp* ctx,
        uint32_t inspeed,
        uint32_t outspeed,
        const char* user,
        const char* ruser,
        const char* term
);

/**
 * Send a window size change notification. It sends a WINCH packet to
 * the server indicating for terminal window size change. If the server
 * doesn't support this it may silently ignore this packet.
 * 
 * @param client instance to operate on
 * @param cols number of columns in size
 * @param rows number of rows in size
 * 
 * @note Client instance must be initialised.
 */
int evycp_client_send_winch(
        struct evycp* ctx,
        uint16_t cols,
        uint16_t rows
);

/**
 * Create a server instance. It also yields a filter bufferevent like client.
 * And registers event callbacks of events (in future dedicated functions
 * might exist for setting these).
 * 
 * @param underlying bufferevent to wrap over
 * @param filter filter that is created
 * @param filter_options filter options if any @see buffervent_filter_new()
 * 
 * @param on_init callback when server is in initialised state
 * @param on_init_arg argument to pass to on_init
 * 
 * @param on_winch callback for window change events
 * @param on_winch_arg argument to pass to on_winch
 * 
 * @note The server may send data to client without it being initialised.
 * It is wise to move any preliminary session tasks to on_init callback.
 */
struct evycp* evycp_server_new(
        struct bufferevent* underlying,
        struct bufferevent** filter,
        int filter_options,

        init_cb_t on_init,
        void* on_init_arg,

        winch_cb_t on_winch,
        void *on_winch_arg
);
#endif
#ifdef __cplusplus
}
#endif