#ifndef YUTILS_H
#define YUTILS_H
#include <string.h>
#include <stddef.h>

struct strnsplit {
    const char* ptr;
    size_t      len;
};

static const char* strnsplit(
        const char* restrict str,
        int c,
        size_t len,
        size_t *part_len,
        struct strnsplit* ctx
) {
    if(str != NULL) {
        ctx->ptr = str;
        ctx->len = len;
    }

    if(ctx->len == 0) {
        *part_len = 0;
        return NULL;
    }

    const char* begin = ctx->ptr;

    do {
        --ctx->len;
        if(*ctx->ptr++ == c)
            break;
    } while(ctx->len > 0);

    *part_len = ctx->ptr - begin;
    return begin;
}
#endif