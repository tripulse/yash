#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#include <event2/buffer.h>
#include <event2/bufferevent.h>

#include "ycp.h"
#include "yutils.h"


struct evycp {
    enum evycp_instance {
        EVYCP_TYPE_SERVER,
        EVYCP_TYPE_CLIENT
    } type;

    bool is_init;
    struct evbuffer* output;

    size_t data_left;

    /* This part applies to servers only. */
    winch_cb_t on_winch;
    void* on_winch_arg;

    init_cb_t on_init;
    void* on_init_arg;
};


#define min(a,b) ((a) < (b) ? (a) : (b))

static enum bufferevent_filter_result evycp_read_cb(
    struct evbuffer *src,
    struct evbuffer *dst,
    ev_ssize_t dst_limit,
    enum bufferevent_flush_mode mode,
    void *arg
) {
    char hdr[3];
    int ret;

    size_t buflen;
    uint16_t pktlen;

    struct evycp* ctx = arg;
    size_t read_limit = (size_t)dst_limit;

    if(mode == BEV_FINISHED)
        return BEV_OK;

    // this is to read the DATA packet progressively so it
    // can be instantly read without the full packet arriving.
    if(ctx->data_left > 0) {
        ret = evbuffer_remove_buffer(src, dst, min(pktlen, read_limit));
        if(ret < 0)
            return BEV_ERROR;

        ctx->data_left -= ret;
    }

    buflen = evbuffer_get_length(src);
    if(buflen == 0)
        return BEV_OK;

    ret = evbuffer_copyout(src, hdr, 3);
    if(ret < 0)
        return BEV_ERROR;
    else if(ret < 3)
        return BEV_NEED_MORE;

    evbuffer_drain(src, 3);
    buflen -= 3;

    pktlen  = (uint8_t)hdr[1] << 8 |
              (uint8_t)hdr[2];

    // for others packets as they're much smaller and mostly
    // infrequent than the DATA packets require the whole
    // packet to be arrived.
    if(buflen < pktlen && hdr[1] != 0x0)
        return BEV_NEED_MORE;

    // if the INIT packet is sent later than other, or is sent
    // twice that is a protocol error.
    if(  ctx->type == EVYCP_TYPE_SERVER &&
         ((hdr[0] != 0x2 && !ctx->is_init)||
          (hdr[0] == 0x2 &&  ctx->is_init)))
        return BEV_ERROR;

    switch(hdr[0]) {
    /* DATA */
    case 0x0:
        ctx->data_left = pktlen;

        ret = evbuffer_remove_buffer(src, dst, min(pktlen, read_limit));
        if(ret < 0)
            return BEV_ERROR;

        ctx->data_left -= ret;
        break;

    /* WINCH */
        char winch_hdr[4];
        uint16_t columns,
                 rows;
    case 0x1:
        ret = evbuffer_remove(src, winch_hdr, 4);
        if(ret < 4 || ctx->type != EVYCP_TYPE_SERVER)
            return BEV_ERROR;

        columns =   (uint8_t)winch_hdr[0] << 8 |
                    (uint8_t)winch_hdr[1];
        rows    =   (uint8_t)winch_hdr[2] << 8 |
                    (uint8_t)winch_hdr[3];

        if(ctx->on_winch)
            ctx->on_winch(columns, rows, ctx->on_winch_arg);
        break;

    /* INIT */
        char conf_hdr[8];
        uint32_t ispeed, ospeed;

        static char conf_footer[65527 + 1];
        const char  *conf_user,
                    *conf_ruser,
                    *conf_term;
        const void* ret2;

        size_t           part_len;
        struct strnsplit splitctx;
    case 0x2:
        ret = evbuffer_remove(src, conf_hdr, 8);
        if(ret < 8 || ctx->type != EVYCP_TYPE_SERVER)
            return BEV_ERROR;

        ispeed =    (uint8_t)conf_hdr[0] << 24 |
                    (uint8_t)conf_hdr[1] << 16 |
                    (uint8_t)conf_hdr[2] << 8  |
                    (uint8_t)conf_hdr[3];

        ospeed =    (uint8_t)conf_hdr[4] << 24 |
                    (uint8_t)conf_hdr[5] << 16 |
                    (uint8_t)conf_hdr[6] << 8  |
                    (uint8_t)conf_hdr[7];

        pktlen -= 8;
        ret = evbuffer_remove(src, conf_footer, pktlen);
        if(ret < 0)
            return BEV_ERROR;

        conf_user = strnsplit(conf_footer, '\0', pktlen, &part_len, &splitctx);
        pktlen -= part_len;

        conf_ruser = strnsplit(NULL, '\0', pktlen, &part_len, &splitctx);
        pktlen -= part_len;

        conf_term = strnsplit(NULL, '\0', pktlen, &part_len, &splitctx);
        pktlen -= part_len;

        // if some data was remaining just drain it. this data should not
        // exist as of the specification but just ignore it.
        if(pktlen) {
            ret = evbuffer_drain(src, pktlen);
            if(ret < pktlen)
                return BEV_ERROR;
        }

        if(!conf_user || !conf_ruser || !conf_term)
            return BEV_ERROR;

        if(ctx->on_init)
            ctx->on_init(ispeed,
                         ospeed,
                         conf_user,
                         conf_ruser,
                         conf_term,
                         ctx->on_init_arg);

        ctx->is_init = true;
        break;
    default:
        evbuffer_drain(src, pktlen);
    }

    return BEV_OK;
}

static enum bufferevent_filter_result evycp_write_cb(
    struct evbuffer *src,
    struct evbuffer *dst,
    ev_ssize_t dst_limit,
    enum bufferevent_flush_mode mode,
    void *arg
) {
    char hdr[3];
    int ret;

    size_t buflen;
    uint16_t pktlen;

    struct evycp* ctx = arg;

    // if the client hasn't initialised from its side sending an
    // INIT packet to the server, then it is a protocol violation.
    if(ctx->type == EVYCP_TYPE_CLIENT && !ctx->is_init)
        return BEV_ERROR;

    if(mode == BEV_FINISHED)
        return BEV_OK;

    buflen = evbuffer_get_length(src);
    if(buflen == 0)
        return BEV_OK;

    if(mode == BEV_NORMAL)
        pktlen = min(buflen, min(dst_limit, 65535));
    else if(mode == BEV_FLUSH)
        pktlen = min(buflen, 65535);

    // type (DATA)
    hdr[0] = 0x0;

    // length
    hdr[1] = pktlen >> 8;
    hdr[2] = pktlen & 0xFF;

    // payload
    ret  = evbuffer_add(dst, hdr, 3);
    ret += evbuffer_remove_buffer(src, dst, pktlen);

    if(ret < pktlen)
        return BEV_ERROR;

    return BEV_OK;
}

struct evycp* evycp_client_new(
        struct bufferevent* underlying,
        struct bufferevent** filter,
        int filter_options
) {
    struct evycp* ctx;

    ctx = malloc(sizeof(struct evycp));
    if(ctx == NULL)
        return NULL;

    ctx->type = EVYCP_TYPE_CLIENT;
    ctx->is_init = false;
    ctx->output = bufferevent_get_output(underlying);

    *filter = bufferevent_filter_new(
                underlying,
                evycp_read_cb,
                evycp_write_cb,
                filter_options,
                free, ctx);

    if(*filter == NULL)
        return NULL;

    return ctx;
}

struct evycp* evycp_server_new(
        struct bufferevent* underlying,
        struct bufferevent** filter,
        int filter_options,

        init_cb_t on_init,
        void* on_init_arg,

        winch_cb_t on_winch,
        void *on_winch_arg
) {
    struct evycp* ctx;

    ctx = malloc(sizeof(struct evycp));
    if(ctx == NULL)
        return NULL;

    ctx->type = EVYCP_TYPE_SERVER;
    ctx->is_init = false;
    ctx->output = bufferevent_get_output(underlying);

    ctx->on_winch = on_winch;
    ctx->on_winch_arg = on_winch_arg;
    ctx->on_init = on_init;
    ctx->on_init_arg = on_init_arg;

    *filter = bufferevent_filter_new(
                underlying,
                evycp_read_cb,
                evycp_write_cb,
                filter_options,
                free, ctx);

    if(*filter == NULL)
        return NULL;

    return ctx;
}

int evycp_client_send_winch(
        struct evycp* ctx,

        uint16_t cols,
        uint16_t rows
) {
    if(  ctx->type != EVYCP_TYPE_CLIENT ||
        (ctx->type == EVYCP_TYPE_CLIENT && !ctx->is_init))
        return -1;

    char pkt[7];
    int ret;

    // type (WINCH)
    pkt[0] = 0x1;

    // length
    pkt[1] = 0x0;
    pkt[2] = 0x4;

    // columns
    pkt[3] = cols >> 8;
    pkt[4] = cols & 0xFF;

    // rows
    pkt[5] = rows >> 8;
    pkt[6] = rows & 0xFF;

    return evbuffer_add(ctx->output, pkt, 7);
}

int evycp_client_init(
        struct evycp* ctx,

        uint32_t inspeed,
        uint32_t outspeed,

        const char* user,
        const char* ruser,
        const char* term
) {
    char pkt[11];
    int ret;

    size_t  userlen,
            ruserlen,
            termlen;
    uint16_t pktlen;

    // an INIT packet can only be send once.
    if(  ctx->type != EVYCP_TYPE_CLIENT ||
        (ctx->type == EVYCP_TYPE_CLIENT && ctx->is_init))
        return -1;

    userlen  = strlen(user)  + 1;
    ruserlen = strlen(ruser) + 1;
    termlen  = strlen(term)  + 1;

    // it shouldn't be that long, should it?
    if((userlen + ruserlen + termlen) > 65527)
        return -1;

    pktlen = 8 + userlen + ruserlen + termlen;

    // type (INIT)
    pkt[0] = 0x2;

    // length
    pkt[1] = pktlen >> 8;
    pkt[2] = pktlen & 0xFF;

    // inspeed
    pkt[3] =  inspeed >> 24;
    pkt[4] = (inspeed >> 16) & 0xFF;
    pkt[5] = (inspeed >> 8)  & 0xFF;
    pkt[6] =  inspeed        & 0xFF;

    // outspeed
    pkt[7]  =  outspeed >> 24;
    pkt[8]  = (outspeed >> 16) & 0xFF;
    pkt[9]  = (outspeed >> 8)  & 0xFF;
    pkt[10] =  outspeed        & 0xFF;


    ret  = evbuffer_add(ctx->output, pkt,   11);
    ret += evbuffer_add(ctx->output, user,  userlen);  // local username
    ret += evbuffer_add(ctx->output, ruser, ruserlen); // remote username
    ret += evbuffer_add(ctx->output, term,  termlen);  // TERM environ

    ret = ret < 0 ? -1 : 0;
    if(ret == 0)
        ctx->is_init = true;
    return ret;
}
