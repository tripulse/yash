YASH is a remote login shell that is minimalistic by design. It has been designed to perform one-task and perform it correctly
as per the UNIX philosophy. That is to provide a shell (specific to the user logged in) to control a remote machine over a reliable (TCP) and encrypted (TLS) connection and authenticate the session (PAM).

This is a reference implementation of YASH done using asynchronous programming paradigm of [libevent][2], to allow for cross-platform and clean-code with less resource overhead induced by utilising the forking or threading paradigm, the program is IO bounded not CPU bounded so both the server and the client are single threaded intentionally by that means.

### Features

- Auto-generated X.509 certificates.
- To-the-point bloatless protocol.
- Ease of configuration.
- Cross UNIX compatibility.

---
### SSH

A well-known tool in the UNIX world, though its purpose extends far that of a remote shell to provide for services whose solutions already exist and are well-suited being seperate than the protocol (eg. tunneling, port forwarding). <sup>[1]</sup> SSH re-invents the wheel by infesting the security part in the protocol itself than to use a underlying one like SSL or TLS, which are widely and subconciously being used throughout all the internet.

### Rlogin
Though to-the-point but deperecated in the days of secure communications.


[1]: https://tripulsive.blogspot.com/2021/08/things-wrong-with-openssh.html
[2]: https://libevent.org/
